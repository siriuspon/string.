#include <iostream>
#include <string>

int main()
{
	std::string text = "Hello SkillBox";

	std::cout << text << "\n";
	std::cout << "Line length = " << text.length() << "\n";
	std::cout << "First character - " << text.front() << "\n";
	std::cout << "Last character - " << text.back() << "\n";

}
